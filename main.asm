section .text
%include "colon.inc"


section .data
%include "words.inc"
err1: db "Input string is too long",0
err2: db "No such word", 0

section .text

extern read_word
extern find_word
extern print_string
extern print_error
extern string_length
extern print_newline;
extern exit

global _start
_start:
    push r8
    mov r8, rsp
    sub rsp, 256
    mov rdi, rsp
    mov rsi, 255
    call read_word
    cmp rax, 0
    je .too_long
    mov rsi, LAST_LABEL
    mov rdi, rax
    call find_word
    cmp rax, 0
    je .not_found
    add rax, 8
    push rax
    mov rdi, [rsp]
    call string_length
    pop rdi
    add rdi, rax
    add rdi, 1        ;null-termiantor
    call print_string
    jmp .finish

.too_long:
    mov rdi, err1
    call print_error
    jmp .finish

.not_found:
    mov rdi, err2
    call print_error
    jmp .finish

.finish:
    call print_newline
    mov rsp, r8
    pop r8
    mov rdi, 0
    call exit


