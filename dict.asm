global find_word
extern string_equals

section .text

;stores:
;rdi - string pointer
;rsi - map pointer
;returns:
;if success - address of element, else 0
find_word:
    xor rax,rax
.loop:
    cmp rsi, 0
    je .no
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    je .yes
    mov rsi, [rsi]
    jmp .loop
.yes:
    mov rax, rsi
    ret
.no:
    xor rax, rax
    ret
