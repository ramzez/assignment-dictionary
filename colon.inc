%define LAST_LABEL 0

%macro colon 2

elem_%2: dq LAST_LABEL
db %1, 0
value_%2:

%define LAST_LABEL elem_%2

%endmacro
